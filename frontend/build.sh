export npm_lifecycle_event='build'

echo "Clearing 'client' distribution folder.."
rm -rf ../../dist/client
echo "'client' distribution folder cleared!"

echo "Running webpack.."
webpack

mkdir ~/entry_task/frontend/dist/client/css/client
mv ~/entry_task/frontend/dist/client/*.woff ~/entry_task/frontend/dist/client/css/client
mv ~/entry_task/frontend/dist/client/*.woff2 ~/entry_task/frontend/dist/client/css/client
mv ~/entry_task/frontend/dist/client/*.eot ~/entry_task/frontend/dist/client/css/client
mv ~/entry_task/frontend/dist/client/*.ttf ~/entry_task/frontend/dist/client/css/client
