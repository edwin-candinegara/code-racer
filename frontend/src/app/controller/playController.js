import {WS_EVENTS} from '../const';

class PlayController {
    constructor($scope, $interval, $state, WebSocketService, CodeService, StatsService, $mdToast) {
        this.$mdToast = $mdToast;
        this.code = CodeService.getCode();
        this.WebSocketService = WebSocketService;
        this.StatsService = StatsService;
        this.leaderboard = [{'username': '-', 'cpm': 0}];
        this.query = {
            'order': '',
            'limit': 10,
            'page': 1
        };
        this.setupEditor();

        WebSocketService.registerEvent(
            WS_EVENTS.PUSH_LEADERBOARD,
            (data) => this.leaderboard = data.message.leaderboard
        );

        WebSocketService.registerEvent(
            WS_EVENTS.GAME_ENDED,
            (data) => $state.go('summary')
        );

        this.$interval = $interval;
        this.interval = $interval(() => {
            const data = {
                'cpm': this.StatsService.getCpm(),
                'progress': this.StatsService.getProgress(),
                'error': this.StatsService.getError()
            };
            WebSocketService.send(WS_EVENTS.PUSH_PLAYER_STATS, data);
            console.log('player stats', data);
        }, 1000);

        $scope.$on('$destroy', () => {
            $interval.cancel(this.interval);
            document.onkeypress = () => {};
            document.onkeydown = () => {};
        });
    }

    setupEditor() {
        // Constants
        let DELETE_KEY = 'Backspace';
        let ENTER_KEY = 'Enter';
        let RETURN_IMAGE_PATH = '/assets/ic_enter.png';
        let MAXIMUM_WRONG_CHARACTER = 5;

        // Elements
        let debugger_div = document.getElementById('debugger');
        let text_area = document.getElementById('text_area_id');

        // Metrics
        let start_time;
        let map_character = {};
        let total_character = 0;

        // Setup keyboard events
        let checkKeyboard = () => {
            let typing_pointer = 0;
            let correct_pointer = 0;
            let list_character = text_area.children;

            // list_character = getClearListCharacter(list_character);

            changeToGreenSpan(0);

            // capturing delete key down
            document.onkeydown = function (e) {
                e = e || window.event;
                let key = e.key;

                if (key === DELETE_KEY) {
                    if(typing_pointer < total_character) changeToUntyped(typing_pointer);

                    typing_pointer -= 1;
                    if (typing_pointer < total_character && typing_pointer >= 0){
                        while(list_character[typing_pointer].className === 'tab_span'){
                            typing_pointer -= 1;
                            correct_pointer -= 1;
                        }

                        changeToGreenSpan(typing_pointer);
                    }
                }


                // reduce the point when user delete until the correct pointer
                if(typing_pointer < correct_pointer){
                    correct_pointer -= 1;
                }

                if (typing_pointer < 0) {
                    typing_pointer = 0;
                    changeToGreenSpan(0);
                }
                if (correct_pointer < 0) correct_pointer = 0;

                calculatePercentage(correct_pointer);
                calculateCPM(correct_pointer);
            };

            // only capturing the alphanumeric key press
            document.onkeypress = (e) => {
                e = e || window.event;
                let key = e.key;

                if (isCorrectlyTyped(key, list_character[typing_pointer].textContent) && typing_pointer === correct_pointer ) {
                    // cannot continue until all wrong character is correctly typed
                    changeToCorrect(typing_pointer);

                    correct_pointer += 1;
                    typing_pointer += 1;

                    if(typing_pointer < total_character) {
                        // add pointer until there is no tab
                        while (list_character[typing_pointer].className === 'tab_span') {
                            typing_pointer += 1;
                            correct_pointer += 1;
                        }

                        changeToGreenSpan(typing_pointer);
                    }

                } else if(typing_pointer - correct_pointer < MAXIMUM_WRONG_CHARACTER){
                    // maximum type 5 wrong character
                    changeToIncorrect(typing_pointer);

                    typing_pointer += 1;
                    this.StatsService.setError(this.StatsService.getError() + 1);

                    // add pointer until there is no tab
                    if (typing_pointer < total_character && typing_pointer >= 0){
                        while(list_character[typing_pointer].className === 'tab_span'){
                            typing_pointer += 1;
                            correct_pointer += 1;
                        }
                    }

                    changeToGreenSpan(typing_pointer);
                }

                calculatePercentage(correct_pointer);
                calculateCPM(correct_pointer);
            };

            function isCorrectlyTyped(key, character){
                // \n element is empty in the span
                if(key === ENTER_KEY && character.length === 0) return true;
                else if(key === character) return true;
                else return false;
            }

            function changeToCorrect(index) {
                list_character[index].className = list_character[index].className.replace(/^(new_line_span|untyped|green)$/, 'correct');
            }

            function changeToIncorrect(index) {
                list_character[index].className = list_character[index].className.replace(/^(return_logo_untyped|untyped|green)$/, 'incorrect');
            }

            function changeToUntyped(index) {
                // do not change the color of enter logo
                if (!list_character[index].textContent){
                    list_character[index].className = list_character[index].className.replace(/^(incorrect|green|correct)$/, 'return_logo_untyped');
                    return;
                }
                list_character[index].className = list_character[index].className.replace(/^(incorrect|green|correct)$/, 'untyped');
            }

            function changeToGreenSpan(index){
                list_character[index].className = list_character[index].className.replace(/^(return_logo_untyped|incorrect|correct|untyped)$/, 'green')
            }

            let calculateCPM = (index) => {
                // freeze the CPM calculation
                if(this.StatsService.getProgress() >= 1.0) return;

                const cpm = index / (((Date.now() - start_time)/1000)/60);
                this.StatsService.setCpm(cpm);
                return cpm;
            }

            let calculatePercentage = (index) => {
                const progress = index/total_character;
                this.StatsService.setProgress(progress);

                if (progress >= 1.00) {
                    document.onkeypress = () => {};
                    document.onkeydown = () => {};
                    this.WebSocketService.send(WS_EVENTS.PLAYER_DONE, {});
                    this.$interval.cancel(this.interval);
                    this.$mdToast.show(
                        this.$mdToast.simple()
                          .textContent('Congratulations! You have finished the race! Please wait for other players...')
                          .position('top')
                          .hideDelay(5000)
                      );
                }

                return progress
            }

            function getClearListCharacter(list_character){
                let clearListCharacter = [];

                list_character.forEach(character => {
                    if(!(character.className ===  'hljs-function' || character.className === 'hljs-keyword')){
                        clearListCharacter.push(character);
                    }
                });

                return clearListCharacter;
            }
        };

        // Convert raw text to span elements
        let renderTextToSpan = (text) => {
            for (let i = 0; i < text.length; i++) {
                let span = document.createElement('SPAN');
                // for tab and enter
                if(text[i] === '\n'){
                    let image_return = document.createElement('IMG');
                    let br_element = document.createElement('BR');

                    image_return.setAttribute('src', RETURN_IMAGE_PATH);
                    image_return.classList.add('ic_return')
                    span.classList.add('return_logo_untyped');

                    span.append(image_return);
                    span.append(br_element);
                }else if(text[i] === '\t'){
                    span.classList.add('tab_span');
                }else{
                    // this is only for alphanumeric character
                    let content = document.createTextNode(text[i]);
                    span.classList.add('untyped');

                    span.append(content);
                }

                text_area.appendChild(span);
            }
        };

        function startTimer(){
            start_time = Date.now();
        }

        let init = () => {
            total_character = this.code.length;

            renderTextToSpan(this.code);
            hljs.highlightBlock(document.querySelector('pre code'));
            startTimer();
            // startWordMapping(text_test);
            checkKeyboard();
        };

        init();
    }
}

PlayController.$inject = ['$scope', '$interval', '$state', 'WebSocketService', 'CodeService', 'StatsService', '$mdToast'];

export default PlayController;
