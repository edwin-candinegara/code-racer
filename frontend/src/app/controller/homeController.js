import {WS_EVENTS} from '../const';

class HomeController {
  constructor($scope, $interval, $mdColors, $state, $mdDialog, WebSocketService) {
    this.username = WebSocketService.getUsername();
    this.$mdDialog = $mdDialog;
    this.$state = $state;
    this.WebSocketService = WebSocketService;
    this.buttonDisabled = false;

    const THEME_CHANGE_INTERVAL = 10000;
    const PALETTES = ['blue', 'pink', 'teal', 'light-blue', 'amber', 'indigo', 'orange', 'deepOrange'];
    const HUES = [400, 500, 600, 700];

    let interval;
    let homeContainerElement = document.querySelector('#home-container');

    homeContainerElement.style.transition = 'all ease 0.75s';
    homeContainerElement.style.backgroundColor = 'rgba(33, 150, 243, 1)';

    $scope.$on('$destroy', () => {
        homeContainerElement.style.backgroundColor = $mdColors.getThemeColor('blue-500');
        homeContainerElement.style.transition = 'none';
        $interval.cancel(interval);
    });

    interval = $interval(function () {
        let nextPaletteIndex = Math.floor(Math.random() * PALETTES.length);
        let nextHueIndex = Math.floor(Math.random() * HUES.length);

        let nextBackgroundColor = PALETTES[nextPaletteIndex] + '-' + HUES[nextHueIndex];
        homeContainerElement.style.backgroundColor = $mdColors.getThemeColor(nextBackgroundColor);
    }, THEME_CHANGE_INTERVAL);
  }

  joinGame() {
    if (this.username.length <= 0) {
      let alert = this.$mdDialog
        .alert()
        .clickOutsideToClose(true)
        .title('Username')
        .textContent('Please enter your username')
        .ok('Okay!');

      this.$mdDialog.show(alert);
      return;
    }

    // Handle user registration
    this.WebSocketService.registerEvent(
      WS_EVENTS.RESPONSE_REGISTER,
      (data) => {
        this.buttonDisabled = false;
        const hasGameStarted = data.message.has_game_started;
        const isDuplicate = data.message.is_duplicate;

        if (isDuplicate) {
          let alert = this.$mdDialog
            .alert()
            .clickOutsideToClose(true)
            .title('Username')
            .textContent('Sorry, your username has been used...')
            .ok('Okay!');

          this.$mdDialog.show(alert);
          return;
        }

        if (!hasGameStarted) {
          this.$state.go('gameWait');
          console.log('go to current game waiting room');
        } else {
          this.$state.go('inProgress');
          console.log('go to player waiting room')
        }
      }
    );

    this.buttonDisabled = true;
    this.WebSocketService.registerUser(this.username);
  }
}

HomeController.$inject = ['$scope', '$interval', '$mdColors', '$state', '$mdDialog','WebSocketService'];

export default HomeController;
