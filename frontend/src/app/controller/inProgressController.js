import {WS_EVENTS} from '../const';

class InProgressController {
    constructor($scope, $interval, $state, WebSocketService) {
        this.WebSocketService = WebSocketService;
        this.$state = $state;
        this.leaderboard = [{'username': '-', 'cpm': 0}];
        this.currentDot = 0;
        this.dots = [
            document.querySelector('#in-progress-dot-1'),
            document.querySelector('#in-progress-dot-2'),
            document.querySelector('#in-progress-dot-3'),
        ];

        let interval = $interval(() => {
            if (this.currentDot === this.dots.length) {
                this.currentDot = 0;
                this.dots.forEach((dot) => dot.style.visibility = 'hidden')
            }

            const dot = this.dots[this.currentDot++];
            dot.style.visibility = 'visible';
        }, 500)

        $scope.$on('$destroy', () => {
            $interval.cancel(interval);
        });

        // This assumes the data is sorted already
        this.WebSocketService.registerEvent(
            WS_EVENTS.PUSH_LEADERBOARD,
            (data) => this.leaderboard = data.message.leaderboard
        );

        this.query = {
            'order': '',
            'limit': 10,
            'page': 1
        };
    }

    back() {
        this.$state.go('home');
    }
}

InProgressController.$inject = ['$scope', '$interval', '$state', 'WebSocketService'];

export default InProgressController;
