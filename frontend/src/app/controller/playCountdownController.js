import {WS_EVENTS} from '../const';

class PlayCountdownController {
    constructor($scope, $interval, $state, WebSocketService, $mdColors, CodeService) {
        this.WebSocketService = WebSocketService;
        this.countdown = 3;

        let playCountdownContainerElement = document.querySelector('#play-countdown-container');
        this.WebSocketService.registerEvent(
            WS_EVENTS.GAME_STARTING_COUNTDOWN,
            (data) => {
                this.countdown = data.message.countdown;
                if (this.countdown === 0) {
                    CodeService.setCode(data.message.code);
                    $state.go('play');
                }
            }
        );

        playCountdownContainerElement.style.transition = 'all ease 0.75s';
        playCountdownContainerElement.style.backgroundColor = $mdColors.getThemeColor('red-700');
    }
}

PlayCountdownController.$inject = ['$scope', '$interval', '$state', 'WebSocketService', '$mdColors', 'CodeService'];

export default PlayCountdownController;
