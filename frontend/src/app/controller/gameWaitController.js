import {WS_EVENTS} from '../const';

class GameWaitController {
    constructor($scope, $interval, $state, WebSocketService, $mdColors, StatsService) {
        this.countdown = 45;
        this.WebSocketService = WebSocketService;
        this.currentDot = 0;
        this.nPlayers = 0;
        this.dots = [
            document.querySelector('#game-wait-dot-1'),
            document.querySelector('#game-wait-dot-2'),
            document.querySelector('#game-wait-dot-3'),
        ];

        this.WebSocketService.registerEvent(
            WS_EVENTS.PUSH_PLAYER,
            (data) => {
                this.countdown = data.message.countdown;
                this.nPlayers = data.message.user_count;
                if (this.countdown === 0) {
                    $state.go('playCountdown');
                    this.WebSocketService.send(WS_EVENTS.PLAYER_READY, {});
                    StatsService.clearStats();
                }
            }
        );

        let interval = $interval(() => {
            if (this.currentDot === this.dots.length) {
                this.currentDot = 0;
                this.dots.forEach((dot) => dot.style.visibility = 'hidden')
            }

            const dot = this.dots[this.currentDot++];
            dot.style.visibility = 'visible';
        }, 500)

        const THEME_CHANGE_INTERVAL = 5000;
        const PALETTES = ['blue', 'pink', 'teal', 'light-blue', 'amber', 'indigo', 'orange', 'deepOrange'];
        const HUES = [400, 500, 600, 700];

        let intervalColor;
        let gameWaitContainerElement = document.querySelector('#game-wait-container');

        gameWaitContainerElement.style.transition = 'all ease 0.75s';
        gameWaitContainerElement.style.backgroundColor = 'rgba(33, 150, 243, 1)';

        $scope.$on('$destroy', () => {
            gameWaitContainerElement.style.transition = 'none';
            gameWaitContainerElement.style.backgroundColor = $mdColors.getThemeColor('blue-500');
            $interval.cancel(intervalColor);
            $interval.cancel(interval);
        });

        intervalColor = $interval(function () {
            let nextPaletteIndex = Math.floor(Math.random() * PALETTES.length);
            let nextHueIndex = Math.floor(Math.random() * HUES.length);

            let nextBackgroundColor = PALETTES[nextPaletteIndex] + '-' + HUES[nextHueIndex];
            gameWaitContainerElement.style.backgroundColor = $mdColors.getThemeColor(nextBackgroundColor);
        }, THEME_CHANGE_INTERVAL);
    }
}

GameWaitController.$inject = ['$scope', '$interval', '$state', 'WebSocketService', '$mdColors', 'StatsService'];

export default GameWaitController;
