import {WS_EVENTS} from '../const';

class SummaryController {
    constructor($scope, $interval, $state, WebSocketService, $mdColors, StatsService) {
        this.$state = $state;
        this.cpm = StatsService.getCpm();
        this.progress = StatsService.getProgress() * 100;
        this.error = StatsService.getError();

        const THEME_CHANGE_INTERVAL = 5000;
        const PALETTES = ['blue', 'pink', 'teal', 'light-blue', 'amber', 'indigo', 'orange', 'deepOrange'];
        const HUES = [400, 500, 600, 700];

        let interval;
        let summaryContainerElement = document.querySelector('#summary-container');

        summaryContainerElement.style.transition = 'all ease 0.75s';
        summaryContainerElement.style.backgroundColor = 'rgba(33, 150, 243, 1)';

        $scope.$on('$destroy', () => {
            summaryContainerElement.style.transition = 'none';
            summaryContainerElement.style.backgroundColor = $mdColors.getThemeColor('blue-500');
            $interval.cancel(interval);
        });

        interval = $interval(function () {
            let nextPaletteIndex = Math.floor(Math.random() * PALETTES.length);
            let nextHueIndex = Math.floor(Math.random() * HUES.length);

            let nextBackgroundColor = PALETTES[nextPaletteIndex] + '-' + HUES[nextHueIndex];
            summaryContainerElement.style.backgroundColor = $mdColors.getThemeColor(nextBackgroundColor);
        }, THEME_CHANGE_INTERVAL);
    }

    back() {
        this.$state.go('home');
    }
}

SummaryController.$inject = ['$scope', '$interval', '$state', 'WebSocketService', '$mdColors', 'StatsService'];

export default SummaryController;
