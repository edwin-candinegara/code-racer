import 'angular-material/angular-material.min.css';
import 'angular-material-data-table/dist/md-data-table.min.css'

import angular from 'angular';
import uirouter from 'angular-ui-router';
import 'angular-material';
import 'angular-aria';
import 'angular-messages';
import 'angular-websocket';
import 'angular-material-data-table';

import HomeController from './controller/homeController';
import InProgressController from './controller/inProgressController';
import GameWaitController from './controller/gameWaitController';
import PlayCountdownController from './controller/playCountdownController';
import PlayController from './controller/playController';
import SummaryController from './controller/summaryController';

import WebSocketService from './service/wsService';
import CodeService from './service/codeService';
import StatsService from './service/statsService';

import '../style/app.css';

const MODULE_NAME = 'app';

const config = ['$stateProvider', '$mdThemingProvider', ($stateProvider, $mdThemingProvider) => {
  $stateProvider
    .state('home', {
      url: '',
      template: require('./view/home.html'),
      controller: 'HomeController',
      controllerAs: 'homeCtrl',
    })
    .state('inProgress', {
      url: '/in_progress',
      template: require('./view/inProgress.html'),
      controller: 'InProgressController',
      controllerAs: 'inProgressCtrl',
    })
    .state('gameWait', {
      url: '/game_wait',
      template: require('./view/gameWait.html'),
      controller: 'GameWaitController',
      controllerAs: 'gameWaitCtrl',
    })
    .state('playCountdown', {
      url: '/play_countdown',
      template: require('./view/playCountdown.html'),
      controller: 'PlayCountdownController',
      controllerAs: 'playCountdownCtrl',
    })
    .state('play', {
      url: '/play',
      template: require('./view/play.html'),
      controller: 'PlayController',
      controllerAs: 'playCtrl',
    })
    .state('summary', {
      url: '/summary',
      template: require('./view/summary.html'),
      controller: 'SummaryController',
      controllerAs: 'summaryCtrl',
    });

    $mdThemingProvider.theme('default').primaryPalette('blue').accentPalette('pink');
}];

angular.module(MODULE_NAME, [uirouter, 'ngMaterial', 'ngMessages', 'angular-websocket', 'md.data.table'])
  // Controllers
  .controller('HomeController', HomeController)
  .controller('InProgressController', InProgressController)
  .controller('GameWaitController', GameWaitController)
  .controller('PlayCountdownController', PlayCountdownController)
  .controller('PlayController', PlayController)
  .controller('SummaryController', SummaryController)

  // Services
  .service('WebSocketService', WebSocketService)
  .service('CodeService', CodeService)
  .service('StatsService', StatsService)

  // Configurations
  .config(config);

export default MODULE_NAME;
