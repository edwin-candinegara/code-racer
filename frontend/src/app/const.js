export const WS_URL = 'ws://159.89.140.125/api/ws';

export const WS_EVENTS = {
    RECONNECT: 'reconnect',
    REGISTER_CONNECTION: 'register_connection',
    REGISTER: 'register',
    RESPONSE_REGISTER: 'response_register',
    PUSH_LEADERBOARD: 'push_leaderboard',
    GAME_ENDED: 'game_ended',
    PUSH_PLAYER: 'push_player',
    GAME_STARTING_COUNTDOWN: 'game_starting_countdown',
    PLAYER_READY: 'player_ready',
    PUSH_PLAYER_STATS: 'push_player_stats',
    PLAYER_DONE: 'player_done',
};
