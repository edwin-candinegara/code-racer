export default () => (value, max) => {
  if (!value) return '';

  const maxInt = parseInt(max, 10);
  if (!maxInt) return value;
  if (value.length <= maxInt) return value;

  const valueCut = value.substr(0, maxInt);
  return `${valueCut} ...`;
};
