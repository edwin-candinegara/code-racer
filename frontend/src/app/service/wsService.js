import {WS_URL, WS_EVENTS} from '../const';


class WebSocketService {
  constructor($websocket) {
    this.$websocket = $websocket;
    this.connection = null;
    this.callbacks = {};
    this.username = '';
    this.retryCount = 0;
  }

  getUsername() {
    return this.username;
  }

  connect() {
    console.log('Connecting...')
    this.connection = this.$websocket(WS_URL, '', {'reconnectIfNotNormalClose': true});
    this.connection.onOpen(() => {
      console.log('Websocket opened!');
    });

    this.connection.onMessage((message) => {
      let data;
      data = JSON.parse(message.data);

      if (data.message.length <= 0) {
        data.message = {}
      } else {
        data.message = JSON.parse(data.message);
      }

      console.log(data);
      if (this.callbacks[data.type]) this.callbacks[data.type](data);
    });
  }

  send(eventName, data) {
    if (!this.connection) this.connect();

    const finalData = {
      'message': JSON.stringify(data),
      'type': eventName,
      'username': this.username
    };

    this.connection.send(JSON.stringify(finalData));
  }

  registerEvent(eventName, callback) {
    this.callbacks[eventName] = callback;
  }

  unregisterEvent(eventName) {
    delete this.callbacks[eventName];
  }

  registerUser(username) {
    this.username = username && username.length > 0 ? username : this.username;
    this.send(WS_EVENTS.REGISTER, {});
  }
}

WebSocketService.$inject = ['$websocket'];

export default WebSocketService;
