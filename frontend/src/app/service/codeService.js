import {WS_URL, WS_EVENTS} from '../const';


class CodeService {
  constructor() {
    this.code = "def some_function_name():\n" +
    "\tcoin_info = pb_to_dict(extinfo.coin_info)\n";
  }

  getCode() {
    return this.code;
  }

  setCode(code) {
    this.code = code;
  }
}

CodeService.$inject = [];

export default CodeService;
