import {WS_URL, WS_EVENTS} from '../const';


class StatsService {
    constructor() {
        this.cpm = 0;
        this.error = 0;
        this.progress = 0;
    }

    getCpm() {
        return this.cpm;
    }

    getError() {
        return this.error;
    }

    getProgress() {
        return this.progress;
    }

    setCpm(cpm) {
        this.cpm = cpm;
    }

    setError(error) {
        this.error = error;
    }

    setProgress(progress) {
        this.progress = progress;
    }

    clearStats() {
        this.cpm = 0;
        this.error = 0;
        this.progress = 0;
    }
}

StatsService.$inject = [];

export default StatsService;
