export default [() => ({
  restrict: 'E',
  replace: 'true',
  template: require('../view/searchMenu.html'),
  controller: 'SearchMenuController',
  controllerAs: 'searchMenuCtrl',
})];
