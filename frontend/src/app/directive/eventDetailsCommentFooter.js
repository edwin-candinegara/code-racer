export default [() => ({
  restrict: 'E',
  replace: true,
  template: require('../view/eventDetailsCommentFooter.html'),
  controller: 'EventDetailsCommentFooterController',
  controllerAs: 'eventDetailsCommentFooterCtrl',
})];
