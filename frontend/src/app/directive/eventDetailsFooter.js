export default [() => ({
  restrict: 'E',
  replace: true,
  template: require('../view/eventDetailsFooter.html'),
  controller: 'EventDetailsFooterController',
  controllerAs: 'eventDetailsFooterCtrl',
})];
