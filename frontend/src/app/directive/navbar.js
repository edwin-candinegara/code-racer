export default [() => ({
  restrict: 'E',
  replace: true,
  template: require('../view/navbar.html'),
  controller: 'NavbarController',
  controllerAs: 'navbarCtrl',
})];
