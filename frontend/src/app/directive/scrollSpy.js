import angular from 'angular';
import { SPYSCROLL_UPPER_LIMIT, SPYSCROLL_LOWER_LIMIT } from '../const';

export default [() => ({
  restrict: 'A',
  scope: {
    containerId: '@containerId',
    tabName: '@tabName',
    tracker: '=tracker',
  },
  link: (scope, el) => {
    const containerEl = angular.element(document.getElementById(scope.containerId));
    const process = () => {
      const top = el[0].getBoundingClientRect().top;
      if (top < SPYSCROLL_UPPER_LIMIT && top > SPYSCROLL_LOWER_LIMIT) {
        scope.tracker = scope.tabName;
        scope.$apply();
      }
    };

    process();
    containerEl.bind('scroll', process);
  },
})];
