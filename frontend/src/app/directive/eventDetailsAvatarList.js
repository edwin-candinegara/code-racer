export default [() => ({
  restrict: 'E',
  replace: true,
  scope: {
    type: '@type',
    full: '=full',
  },
  template: require('../view/eventDetailsAvatarList.html'),
  controller: 'EventDetailsAvatarListController',
  controllerAs: 'eventDetailsAvatarListCtrl',
})];
