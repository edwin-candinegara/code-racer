export default [() => ({
  restrict: 'E',
  replace: true,
  template: require('../view/eventListItem.html'),
  scope: {
    event: '=event',
  },
  controller: 'EventListItemController',
  controllerAs: 'eventListItemCtrl',
})];
