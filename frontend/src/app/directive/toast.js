export default [() => ({
  restrict: 'E',
  replace: true,
  scope: {
    delay: '=delay',
  },
  template: require('../view/toast.html'),
  controller: 'ToastController',
  controllerAs: 'toastCtrl',
})];
