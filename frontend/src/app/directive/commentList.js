export default [() => ({
  restrict: 'E',
  replace: true,
  template: require('../view/commentList.html'),
  controller: 'CommentListController',
  controllerAs: 'commentListCtrl',
})];
