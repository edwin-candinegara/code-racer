// Base
export const API_BASE_URL = 'https://192.168.56.102/api';

// User
export const USER_API_URL = `${API_BASE_URL}/user`;
export const USER_LOGIN_API_URL = `${USER_API_URL}/login`;
export const USER_GET_PROFILE_URL = `${USER_API_URL}/profile`;
export const USER_GET_PROFILE_BY_IDS_URL = `${USER_API_URL}/usernames_avatars`;
export const buildGetRelevantEventsByUserId = userId => `${USER_API_URL}/${userId}/events`;

// Event
export const EVENT_API_URL = `${API_BASE_URL}/event`;
export const GET_ALL_RELEVANT_CHANNEL_URL = `${EVENT_API_URL}/channels/all`;
export const GET_CHANNEL_BY_IDS_URL = `${EVENT_API_URL}/channels/partial`;
export const GET_EVENT_LIST_URL = `${EVENT_API_URL}/list`;
export const TOGGLE_EVENT_LIKES_URL = `${EVENT_API_URL}/likes`;
export const TOGGLE_EVENT_GOING_URL = `${EVENT_API_URL}/participants`;
export const SUBMIT_COMMENT_URL = `${EVENT_API_URL}/comments`;
export const buildGetEventDetails = eventId => `${EVENT_API_URL}/${eventId}`;
export const buildGetCommentsByEventId = eventId => `${EVENT_API_URL}/${eventId}/comments`;
export const buildGetParticipantIdsByEventId = eventId => `${EVENT_API_URL}/${eventId}/participants`;
export const buildGetLikerIdsByEventId = eventId => `${EVENT_API_URL}/${eventId}/likes`;
