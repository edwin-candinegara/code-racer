# Code Racer
Code racer is MMO typing race, where everyone can compete in typing programming language (e.g. python) to increase the familiarity to the language syntax while having fun competing with other player :).

**[Try it!](https://tinyurl.com/coderacer)**

## How to play
- Enter your name.
![](public/enter_name.png)
- Wait for other players to join.
![](public/waiting_player.png)
- Type the code!
![](public/in_game.png)
- See your performance.
![](public/performance.png)
- Repeat!

**Notes**:
If you are late do not worry, you can wait for the next race while watching the leader board :)
![](public/waiting_room.png)

## Referrence
- [Slides](https://docs.google.com/presentation/d/1WhNn0yl3DdFw8pu_dgEHPBbxniFwElb0iwbwoo4FWzc/edit?usp=sharing)
- [Frontend](frontend/README.md)
- [Backend](goblog-backend/README.md)