package utils

import (
	"fmt"
	"log"
)

const (
	LOG_DETAIL  = 0
	LOG_WARNING = 1
	LOG_ERROR   = 2
	LOG_DISABLE = 3
)

var (
	LogLevel = LOG_DETAIL
)

const (
	PRINT_DETAIL  = "DETAIL"
	PRINT_WARNING = "WARNING"
	PRINT_ERROR   = "ERROR"
)

// FormattedPrint ...
func FormattedPrint(printType string, msg string) {
	log.Printf("[%s] %s\n", printType, msg)
}

// PrintDetail ...
func PrintDetail(msg string) {
	if LogLevel <= LOG_DETAIL {
		FormattedPrint(PRINT_DETAIL, msg)
	}
}

// PrintWarning ...
func PrintWarning(msg string) {
	if LogLevel <= LOG_WARNING {
		FormattedPrint(PRINT_WARNING, msg)
	}
}

// PrintError ...
func PrintError(msg string) {
	if LogLevel <= LOG_ERROR {
		FormattedPrint(PRINT_ERROR, msg)
	}
}

// PrintDetailf ...
func PrintDetailf(msg string, v ...interface{}) {
	PrintDetail(fmt.Sprintf(msg, v...))
}

// PrintWarningf ...
func PrintWarningf(msg string, v ...interface{}) {
	PrintWarning(fmt.Sprintf(msg, v...))
}

// PrintErrorf ...
func PrintErrorf(msg string, v ...interface{}) {
	PrintError(fmt.Sprintf(msg, v...))
}
