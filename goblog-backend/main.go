package main

import (
	"flag"
	"fmt"
	"net/http"

	"./utils"

	"github.com/gorilla/websocket"
)

var (
	port            = flag.Int("port", 8000, "Port to serve")
	textDir         = flag.String("textdir", "./text", "Directory of code snipets")
	logFile         = flag.String("log", "goblog-backend.log", "Log file location")
	tickDuration    = flag.Int("tick", 1000, "engine tick duration (ms)")
	waitCD          = flag.Int("waitcd", 60, "Waiting countdown for player (tick)")
	readyCD         = flag.Int("readycd", 3, "Ready countdown for player (tick)")
	raceCD          = flag.Int("racecd", 60, "Ready countdown for player (tick)")
	leaderboardSize = flag.Int("leader", 10, "Leaderboard size that is sent to client")
	logLevel        = flag.Int("loglevel", 0, "Log level 0=DETAIL, 1=WARNING, 2=ERROR")
)

// Message type
const (
	// Request
	REQUEST_REGISTER          = "register"
	REQUEST_PLAYER_READY      = "player_ready"
	REQUEST_PUSH_PLAYER_STATS = "push_player_stats"
	REQUEST_PLAYER_DONE       = "player_done"

	// Response
	RESPONSE_REGISTER = "response_register"

	// Push
	PUSH_LEADERBOARD             = "push_leaderboard"
	PUSH_GAME_ENDED              = "game_ended"
	PUSH_PLAYER                  = "push_player"
	PUSH_GAME_STARTING_COUNTDOWN = "game_starting_countdown"
)

// Configure the upgrader
var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// GoblogRequestMessage define each goblog client struct
type GoblogRequestMessage struct {
	MsgType  string `json:"type"`
	Username string `json:"username"`
	Message  string `json:"message"`
}

func main() {
	flag.Parse()

	utils.LogLevel = *logLevel

	// // Open log file
	// f, err := os.OpenFile(*logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	// if err != nil {
	// 	utils.PrintErrorf("Error in opening log: %v", err)
	// }
	// defer f.Close()

	// DEBUG: reate a simple file server
	fs := http.FileServer(http.Dir("./public"))
	http.Handle("/", fs)

	// Configure websocket route
	http.HandleFunc("/ws", handleConnections)

	// Start game engine
	go messageDispatcher()

	// Start the server on localhost port 8000 and log any errors
	portStr := fmt.Sprintf(":%d", *port)
	utils.PrintDetailf("Listen and serve port%s", portStr)
	err := http.ListenAndServe(portStr, nil)
	if err != nil {
		utils.PrintErrorf("ListenAndServe: %v", err)
	}
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	enableCors(&w)
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		utils.PrintErrorf("Error in upgrading request into websocket: %v", err)
	}
	defer ws.Close()

	for {
		var msg GoblogRequestMessage
		// Read in a new message as JSON and map it to a GoblogRequestMessage object
		err := ws.ReadJSON(&msg)
		if err != nil {
			utils.PrintErrorf("Error in reading message: %v", err)
			continue
		}

		// Handle message
		Handle(msg, ws)
	}
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}
