package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path"
	"sort"
	"time"

	"./utils"
	"github.com/gorilla/websocket"
)

// GoblogClient define goblog client struct
type GoblogClient struct {
	Username string
	IsReady  bool
	IsDone   bool
	Rank     int
	Stats    *GoblogUserStats
	WS       *websocket.Conn
}

// GoblogUserStats define user stats struct
type GoblogUserStats struct {
	Cpm         float64 `json:"cpm"`
	Progress    float32 `json:"progress"`
	TypingError int     `json:"error"`
}

// Connected clients
var inGameClients = make(map[string]*GoblogClient)   // in game clients
var watchingClients = make(map[string]*GoblogClient) // watching clients

// code text
var codeTexts = []string{}

// Game status
var (
	isStarted              = false
	raceCount              = 0
	playerWaitingCountdown = 0
	playerReadyCountdown   = 0
	playerInGameCountDown  = 0
)

func messageDispatcher() {
	utils.PrintDetailf("Engine message discpatcher starter")
	readyChan := make(chan bool)
	err := initDispatcher()
	if err != nil {
		utils.PrintErrorf("Error in init dispatcher: %v", err)
		return
	}

	for {
		go waitTimer(readyChan)

		if len(inGameClients) > 0 {
			if isStarted {
				sendPushLeaderboard()
				if playerInGameCountDown >= 0 {
					if isAllPlayerDone() {
						endGame()
						<-readyChan
						continue
					}
					playerInGameCountDown--
				} else {
					endGame()
				}
			} else {
				if playerWaitingCountdown >= 0 {
					sendPushPlayer()
					playerWaitingCountdown--
				} else if playerReadyCountdown >= 0 {
					// if !isAllPlayerReady() {
					// 	<-readyChan
					// 	continue
					// }
					sendGameStartingCountdown()
					playerReadyCountdown--
				} else {
					isStarted = true
					playerInGameCountDown = *raceCD
				}
			}
		} else {
			utils.PrintDetailf("Race: %d Waiting for first player ...", raceCount)
		}

		<-readyChan
	}
}

func initDispatcher() (err error) {
	raceCount++
	isStarted = false
	playerWaitingCountdown = *waitCD
	playerReadyCountdown = *readyCD
	err = openAllTextFile()
	return
}

func openAllTextFile() (err error) {
	utils.PrintDetailf("Loading text file from directory:%s", *textDir)
	files, err := ioutil.ReadDir(*textDir)
	if err != nil {
		utils.PrintErrorf("Error in opening text directory: %v", err)
		return err
	}

	for _, f := range files {
		filePath := path.Join(*textDir, f.Name())
		utils.PrintDetailf("Opening text file from:%s", filePath)
		file, err := os.Open(filePath)
		if err != nil {
			utils.PrintErrorf("Error in opening text file: [%v]", err)
			return err
		}
		defer file.Close()

		b, err := ioutil.ReadAll(file)
		codeTexts = append(codeTexts, string(b))
	}
	return
}

func waitTimer(readyChan chan<- bool) {
	utils.PrintDetailf("Start waiting %dms tick...", *tickDuration)
	time.Sleep(time.Duration(*tickDuration) * time.Millisecond)
	utils.PrintDetailf("Stop waiting %dms tick...", *tickDuration)
	readyChan <- true
	return
}

func isAllPlayerDone() bool {
	for _, client := range inGameClients {
		if !client.IsDone {
			return false
		}
	}
	return true
}

func isAllPlayerReady() bool {
	for _, client := range inGameClients {
		if !client.IsReady {
			return false
		}
	}
	return true
}

func endGame() {
	sendGameEnded()
	// send statistic
	clearClient()
	isStarted = false
	playerWaitingCountdown = *waitCD
	playerReadyCountdown = *readyCD
	raceCount++
}

func clearClient() {
	inGameClients = make(map[string]*GoblogClient)   // in game clients
	watchingClients = make(map[string]*GoblogClient) // watching clients
}

// RespPushLeaderboard ..,
type RespPushLeaderboard struct {
	Leaderboard []*RespLeaderboard `json:"leaderboard"`
}

// RespLeaderboard ...
type RespLeaderboard struct {
	Username string  `json:"username"`
	Cpm      float64 `json:"cpm"`
}

type leaderBoard []*RespLeaderboard

func (v leaderBoard) Len() int           { return len(v) }
func (v leaderBoard) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v leaderBoard) Less(i, j int) bool { return v[i].Cpm > v[j].Cpm }

func sendPushLeaderboard() {
	utils.PrintDetailf("sendPushLeaderboard countdown:%d", playerInGameCountDown)

	clientList := leaderBoard{}

	for _, client := range inGameClients {
		newEntry := &RespLeaderboard{
			Username: client.Username,
			Cpm:      client.Stats.Cpm,
		}
		clientList = append(clientList, newEntry)
	}

	sort.Sort(clientList)
	if len(clientList) > *leaderboardSize {
		clientList = clientList[:*leaderboardSize]
	}

	respStruct := RespPushLeaderboard{
		Leaderboard: clientList,
	}

	jsonResp, err := json.Marshal(respStruct)
	if err != nil {
		utils.PrintErrorf("Error in marshalling: %v", err)
		return
	}

	resp := &GoblogResponseMessage{
		MsgType: PUSH_LEADERBOARD,
		Message: string(jsonResp),
	}

	for _, client := range inGameClients {
		sendMessage(*resp, client.WS)
	}

	for _, client := range watchingClients {
		sendMessage(*resp, client.WS)
	}

	return
}

// RespPushPLayer ..,
type RespPushPLayer struct {
	UserCount int `json:"user_count"`
	Countdown int `json:"countdown"`
}

func sendPushPlayer() {
	utils.PrintDetailf("sendPushPlayer usercount:%d, countdown:%d", len(inGameClients), playerWaitingCountdown)
	respStruct := RespPushPLayer{
		UserCount: len(inGameClients),
		Countdown: playerWaitingCountdown,
	}

	jsonResp, err := json.Marshal(respStruct)
	if err != nil {
		utils.PrintErrorf("Error in marshalling: %v", err)
		return
	}

	resp := &GoblogResponseMessage{
		MsgType: PUSH_PLAYER,
		Message: string(jsonResp),
	}

	for _, client := range inGameClients {
		sendMessage(*resp, client.WS)
	}
	return
}

// RespGameStartingCountdown ...
type RespGameStartingCountdown struct {
	Countdown int    `json:"countdown"`
	Code      string `json:"code"`
}

func sendGameStartingCountdown() {
	utils.PrintDetailf("sendGameStartingCountdown: %d", playerReadyCountdown)

	respStruct := RespGameStartingCountdown{
		Countdown: playerReadyCountdown,
		Code:      codeTexts[raceCount%len(codeTexts)],
	}

	jsonResp, err := json.Marshal(respStruct)
	if err != nil {
		utils.PrintErrorf("Error in marshalling: %v", err)
		return
	}

	resp := &GoblogResponseMessage{
		MsgType: PUSH_GAME_STARTING_COUNTDOWN,
		Message: string(jsonResp),
	}

	for _, client := range inGameClients {
		sendMessage(*resp, client.WS)
	}
	return
}

func sendGameEnded() {
	utils.PrintDetailf("sendGameEnded")

	resp := &GoblogResponseMessage{
		MsgType: PUSH_GAME_ENDED,
	}

	for _, client := range inGameClients {
		sendMessage(*resp, client.WS)
	}

	for _, client := range watchingClients {
		sendMessage(*resp, client.WS)
	}

	return
}

// sendMessage to websocket
func sendMessage(msg GoblogResponseMessage, ws *websocket.Conn) {
	utils.PrintDetailf("send message [%v]", msg)
	err := ws.WriteJSON(msg)
	if err != nil {
		utils.PrintErrorf("Error in sending message: %v", err)
	}
	return
}
