package main

import (
	"encoding/json"

	"./utils"
	"github.com/gorilla/websocket"
)

// GoblogResponseMessage define each goblog client struct
type GoblogResponseMessage struct {
	MsgType string `json:"type"`
	Message string `json:"message"`
}

// Handle ...
func Handle(msg GoblogRequestMessage, ws *websocket.Conn) {
	utils.PrintDetailf("New message: %s", msg.MsgType)
	switch msg.MsgType {
	case REQUEST_REGISTER:
		HandleRegister(msg, ws)
	case REQUEST_PLAYER_READY:
		HandlePlayerReady(msg, ws)
	case REQUEST_PUSH_PLAYER_STATS:
		HandlePushPlayerStats(msg, ws)
	case REQUEST_PLAYER_DONE:
		HandlePlayerDone(msg, ws)
	}
	return
}

// GoblogResponseRegister ...
type GoblogResponseRegister struct {
	HasGameStarted bool `json:"has_game_started"`
	IsDuplicate    bool `json:"is_duplicate"`
}

// HandleRegister ...
func HandleRegister(msg GoblogRequestMessage, ws *websocket.Conn) {
	utils.PrintDetailf("HandleRegister [%s] is_started:[%v]", msg.Username, isStarted)

	respRegister := &GoblogResponseRegister{
		HasGameStarted: false,
		IsDuplicate:    false,
	}

	if isStarted {
		respRegister.HasGameStarted = true
		if _, ok := watchingClients[msg.Username]; ok {
			respRegister.IsDuplicate = true
		} else if _, ok := inGameClients[msg.Username]; ok {
			respRegister.IsDuplicate = true
		} else {
			newClient := &GoblogClient{
				Username: msg.Username,
				IsReady:  false,
				IsDone:   false,
				Rank:     0,
				Stats:    &GoblogUserStats{},
				WS:       ws,
			}
			watchingClients[msg.Username] = newClient
		}
	} else {
		if _, ok := inGameClients[msg.Username]; ok {
			respRegister.IsDuplicate = true
		} else if _, ok := watchingClients[msg.Username]; ok {
			respRegister.IsDuplicate = true
		} else {
			newClient := &GoblogClient{
				Username: msg.Username,
				IsReady:  false,
				IsDone:   false,
				Rank:     0,
				Stats:    &GoblogUserStats{},
				WS:       ws,
			}
			inGameClients[msg.Username] = newClient
		}
	}

	jsonResp, err := json.Marshal(respRegister)
	if err != nil {
		utils.PrintErrorf("Error in marshalling for [%s]: %v", msg.Username, err)
		return
	}

	resp := &GoblogResponseMessage{
		MsgType: RESPONSE_REGISTER,
		Message: string(jsonResp),
	}
	sendMessage(*resp, ws)
	return
}

// HandlePlayerReady ...
func HandlePlayerReady(msg GoblogRequestMessage, ws *websocket.Conn) {
	utils.PrintDetailf("HandlePlayerReady [%v]", msg)
	if client, ok := inGameClients[msg.Username]; ok {
		client.WS = ws
		client.IsReady = true
	}
	return
}

// HandlePushPlayerStats ...
func HandlePushPlayerStats(msg GoblogRequestMessage, ws *websocket.Conn) {
	utils.PrintDetailf("HandlePushPlayerStats [%v]", msg)
	if client, ok := inGameClients[msg.Username]; ok {
		client.WS = ws
		newStats := &GoblogUserStats{}
		err := json.Unmarshal([]byte(msg.Message), newStats)
		if err != nil {
			utils.PrintErrorf("Error in marshalling for [%s]: %v", msg.Username, err)
			return
		}
		client.Stats = newStats
	}
	return
}

// HandlePlayerDone ...
func HandlePlayerDone(msg GoblogRequestMessage, ws *websocket.Conn) {
	utils.PrintDetailf("HandlePlayerDone [%v]", msg)
	if client, ok := inGameClients[msg.Username]; ok {
		client.WS = ws
		client.IsDone = true
	}
	return
}
