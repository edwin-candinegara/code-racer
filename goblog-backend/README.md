# GoBlog Backend

## Getting started

#### Build
```bash
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
go get github.com/gorilla/websocket
go build
```

### Run
```bash
go build
./start.sh
```